import Foundation
import Alamofire

public protocol SevAllNetworkLayerProtocol {
    
    static var shared: SevAllNetworkLayer { get }
    var uuid: UUID { get }
    
    func getUser() -> UserAPIModel
}

public struct SevAllNetworkLayer: SevAllNetworkLayerProtocol {
    
    public static let shared = SevAllNetworkLayer()
    
    public let uuid = UUID()
    
    public init() {
    }
    
    public func getUser() -> UserAPIModel {
        .init()
    }
}
