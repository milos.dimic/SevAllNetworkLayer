import XCTest
@testable import SevAllNetworkLayer

final class SevAllNetworkLayerTests: XCTestCase {
    
    private var sut: SevAllNetworkLayer!
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        
        self.sut = SevAllNetworkLayer()
    }
    
    func testExample() throws {
        XCTAssertEqual(self.sut.getUser(), .init())
    }
}
